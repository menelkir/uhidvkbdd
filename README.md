# uhidvkbdd

Driver for USB media keys for FreeBSD.
Related Websites:
* http://althenia.net/uhidvkbdd
* https://github.com/zholos/uhidvkbdd
* https://github.com/zholos/freebsd-ports/tree/master/sysutils/uhidvkbdd
